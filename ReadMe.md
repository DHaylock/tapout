Tapout
===

####Introduction
Is a series of internet connected buttons.

When a button is pressed, it fires a signal to a server and sends a pusher notification to a web page.

####Help
* [Setting up the Hardware](./docs/setuphardware.md)
* [Setting up the Software](./docs/setupsoftware.md)
* [Setting up the Server](./docs/setupserver.md)