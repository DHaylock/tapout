#!/usr/bin/env python
#-------------------------------------------------------
# * Project: Tapout
# * File: tapout.py
# * Author: David Haylock
# * Creation Date: 3-06-2016
# * Copyright: (c) 2016 by David Haylock and Tom Metcalfe
#-------------------------------------------------------
import string
import random
from random import shuffle
import sys
import datetime
import httplib,urllib
import os
import urllib2
import pprint
import os
import subprocess
import time
import json
import requests
from requests.exceptions import HTTPError

#-------------------------------------------------------
# Main Loop
#-------------------------------------------------------
def main_loop():
    
    # Encapsulated the request incase of Network errors
    try:
        r = requests.post(url, data = {'submit':'1','buttonState':'1','buttonId':unitid})
        r.raise_for_status()
    except HTTPError:
        print "Cannot Post Data"
    else:
        print "Sent Data"

    # Let the unit send and receive the data
    time.sleep(7)

    # Switch off the Pi
    os.system("poweroff")


#-------------------------------------------------------
# Init Main
#-------------------------------------------------------
global url
global unitid

# Load the Config File
config = open('config.txt')
url = config.readline().rstrip()
unitid = config.readline().rstrip()

# Print the Unit's data
print "The Host Url is " + url
print "The Units ID is " + unitid

# Delay the program while the 3G Dongle Gets a firm Connection
time.sleep(10)

# Run the Main Loop
main_loop()
