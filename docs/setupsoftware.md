Tapout Software
===

##Python

Inside the Python folder there are two files

* config.txt
* tapout.py

The config file contains the URL we fire the pushed script to aswell as the button id.

The python script will automatically load the config into the app on boot.

The python script will send a simple post request to the specified url, then it will power down the raspberry pi.

##Web