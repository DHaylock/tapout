Tapout Server
===

On your server create a new folder called tapout.
Move the contents of the public folder in this repository into the folder.

##Credentials

Using whatever phpmyadmin create a new database.
Store the password and dbname.

####Database

* Open `tapoutcred.php`
* Add your own credentials

````
<?php
    $dbname = "dbname";
    $dbpasskey = "dbpassword";

    try {
        $DBH = new PDO('mysql:host=localhost;dbname='.$dbname.'',$dbname,$dbpasskey);
    } catch(PDOException $e) {
        echo $e->getMessage();
        exit;
    }
?>
````

####Pusher
* Open `pushercred.php`
* Add your own credentials

````
<?php
    $pusherAppKey = "PUSHER_KEY";
    $pusherAppSecret = "PUSHER_SECRET";
    $pusherAppId = "PUSHER_ID";
?>
````

####Twilio
* Open `twiliocred.php`
* Add your own credentials

````
<?php
    $account_sid = 'Account_SID';
    $auth_token = 'Auth_Token';
?>
````

##Tables
You will need 5 tables.
Copy and Paste the queries below into your SQL Tab.

**Buttons Table**

````
CREATE TABLE IF NOT EXISTS `buttons` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `buttonid` int(10) DEFAULT NULL,
  `buttonstate` varchar(255) NOT NULL,
  `lastused` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
);
````

**Events Table**

````
CREATE TABLE IF NOT EXISTS `events` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `buttonid` int(10) NOT NULL,
  `event` int(1) NOT NULL,
  `action` varchar(255) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
);
````

**Buttons Owner Table**

````
CREATE TABLE IF NOT EXISTS `buttonsowner` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `buttonid` int(10) NOT NULL,
  `ownername` varchar(255) NOT NULL,
  `ownermobile` varchar(255) NOT NULL,
  `friendname` varchar(255) NOT NULL,
  `friendmobile` varchar(255) NOT NULL,
  `message` longtext NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
);
````

**Messages**

````
CREATE TABLE IF NOT EXISTS `messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usermessage` text NOT NULL,
  `friendmessage` text NOT NULL,
  `adminmessage` text NOT NULL,
  PRIMARY KEY (`id`)
);
````

**Admin**

````
CREATE TABLE IF NOT EXISTS `admindata` (
  `id` int(11) NOT NULL,
  `adminname` varchar(255) NOT NULL,
  `adminmobile` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
);
````