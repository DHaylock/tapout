Tapout Hardware
===

####Components
Per unit

* 1x Raspberry Pi Zero
* 1x USB 3G Dongle
* 1x Momentary Button
* 1x USB Power Brick

####Schematic
![Schematic](../images/tapoutschema_schem.png "Schematic")

####Wiring Table

| Button | RPi | Power | MP3 Player|
| --- | --- | --- | --- |
| Pin 0 |  | 5v | |
| Pin 1 | 5v |  | |
|  | 3v3 |  | 3v3 |
|  | GND | GND | GND |

####OS
Tapout is built on Jessie.
The image for the Pi will be copied and distributed to the other Raspberry Pis.

