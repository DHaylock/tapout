$.getJSON("http://tapout.davidhaylock.co.uk/getData.php?getAdmin", function(json1) {
    $.each(json1, function(key, data) {
        var tableContent =
                '<tr>'+
                '<td>'+data.id+'</td>'+
                '<td id="name'+data.id+'">'+data.adminname+'</td>'+
                '<td id="mob'+data.id+'">'+data.adminmobile+'</td>'+
                '<td>'+
                '<button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal" data-messageto="'+data.adminname+'" data-id="'+data.id+'">Update</button>'+
                '</td>'+
                '</tr>';
                $("#tablebody").append(tableContent);
    });
});

$.getJSON("http://tapout.davidhaylock.co.uk/getData.php?getMessages", function(json1) {
    $.each(json1, function(key, data) {
        var tableContent =
                '<tr>'+
                '<td id="actualusermessage">'+data.usermessage+'</td>'+
                '<td id="actualfriendmessage">Your buddy $username '+data.friendmessage+'</td>'+
                '<td id="actualadminmessage"> Hi $adminname, $username '+data.adminmessage+'</td>'+
                '<td>'+
                '<button type="button" class="btn btn-success" data-toggle="modal" data-target="#messageModal" data-messageto="'+data.adminname+'" data-id="'+data.id+'">Update</button>'+
                '</td>'+
                '</tr>';
                $("#messagetablebody").append(tableContent);
    });
});
