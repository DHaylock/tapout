<?php
    if (isset($_POST['submit'])) {

        // Check if there is a Unique Button Id
        if (!isset($_POST['buttonId'])) {
            echo "No Button ID";
            exit;
        }

        // If so import the credentials for MySQL,Pusher and Twilio
        include("tapoutcred.php");

        // Grab the Pusher Library
        require('./pusher/lib/Pusher.php');
        require "./twilio-php/Services/Twilio.php";

        $options = array(
            'cluster' => 'eu',
            'encrypted' => true
        );

        // Twilio Auth
        require_once("twiliocred.php");
        require_once("pushercred.php");

        // Create the Pusher Object
        $pusher = new Pusher(
            $pusherAppKey,
            $pusherAppSecret,
            $pusherAppId,
            $options
        );

        $buttonid = $_POST['buttonId'];
        $buttonState = "";
        $checkButtonValue = $_POST['buttonState'];

        // Self Explainatory
        if (intval($checkButtonValue) == 0) {
            $buttonState = "Released";
        }
        elseif (intval($checkButtonValue) == 1) {
            $buttonState = "Pushed";
        }

        //Check if something exists
        $checkQuery = "SELECT b.buttonid,
                                b.lastused,
                                bt.ownername,
                                bt.ownermobile,
                                bt.friendname,
                                bt.friendmobile,
                                msg.usermessage,
                                msg.friendmessage,
                                msg.adminmessage,
                                (SELECT adminname FROM `admindata` WHERE id = 1) as admin1name,
                                (SELECT adminmobile FROM `admindata` WHERE id = 1) as admin1mobile,
                                (SELECT adminname FROM `admindata` WHERE id = 2) as admin2name,
                                (SELECT adminmobile FROM `admindata` WHERE id = 2) as admin2mobile,
                                (SELECT adminname FROM `admindata` WHERE id = 3) as admin3name,
                                (SELECT adminmobile FROM `admindata` WHERE id = 3) as admin3mobile
                        FROM `buttons` as b
                            JOIN `messages` as msg
                            INNER JOIN `buttonsowner` as bt
                        WHERE bt.buttonid = :buttonid AND b.buttonid = :buttonid";
        $checkResult = $DBH->prepare($checkQuery);
        $checkResult->execute(array(':buttonid' => $buttonid));

        if (!$checkResult) {
            echo "Error: couldn't execute query. ".$checkResult->errorCode();
            exit;
        }

        $numberofResults = $checkResult->rowCount();
        if ($numberofResults == 0) {
            echo "Button Doesn't exist";
            exit;
        }

    	$rows = array();

        $username = "user";
        $usermobilenumber = "";

        $friendname = "friend";
        $friendmobilenumber = "";

        $admindetails = array();

        $usermessage = "";
        $friendmessage = "";
        $adminmessage = "";

    	while ($row = $checkResult->fetch(PDO::FETCH_ASSOC)) {
            $rows[] = $row;

            $username = $row['ownername'];
            $usermobilenumber = $row['ownermobile'];

            $friendname = $row['friendname'];
            $friendmobilenumber = $row['friendmobile'];

            $admindetails = array(
                array("Name" => $row['admin1name'],
                        "Mobile" => $row['admin1mobile']
                    ),
                array("Name" => $row['admin2name'],
                        "Mobile" => $row['admin2mobile']
                    ),
                array("Name" => $row['admin3name'],
                        "Mobile" => $row['admin3mobile']
                    )
            );

            $usermessage = $row['usermessage'];
            $friendmessage = $row['friendmessage'];
            $adminmessage = $row['adminmessage'];

    	}

        // Send info to Pusher
        $data['id'] = $buttonid;
        $data['state'] = $buttonState;
        $data['lastused'] = date("F j, Y, g:i a");
        $pusher->trigger('test_channel', 'my_event', $data);

        // Create the Twilio Object
        $client = new Services_Twilio($account_sid, $auth_token);

        try {
            // User Message
            $client->account->messages->create(array(
                'From' => "+441582214707",
                'To' => $usermobilenumber,
                'Body' => $usermessage,
            ));
        } catch (Services_Twilio_RestException $e) {
            echo $e->getMessage();
        }
        sleep(1);

        try {
            // Friend Message
            $client->account->messages->create(array(
                'From' => "+441582214707",
                'To' => $friendmobilenumber,
                'Body' => "Your buddy ".$username." ".$friendmessage,
            ));
        } catch (Services_Twilio_RestException $e) {
            echo $e->getMessage();
        }
        sleep(1);

        foreach ($admindetails as $detail) {
            try {
                $client->account->messages->create(array(
                    'From' => "+441582214707",
                    'To' => $detail['Mobile'],
                    'Body' => "Hi ".$detail['Name'].", ".$username." ".$adminmessage,
                ));
            } catch (Services_Twilio_RestException $e) {
                echo $e->getMessage();
            }
            sleep(1);
        }
        
        // Update the Status Panel
        $updateQuery = "UPDATE `buttons`
                        SET `buttonstate`= :buttonState
                        WHERE `buttonid`= :buttonid
                        ";

        $updateResult = $DBH->prepare($updateQuery);
        $updateResult->execute(array(':buttonState' => $buttonState,':buttonid' => $buttonid));

        if (!$updateResult) {
            echo "Error: couldn't execute query. ".$updateResult->errorCode();
            exit;
        }

        // Add New event to the events table
        $insertQuery = "INSERT INTO `events` (`action`,`buttonid`) VALUES(:buttonState,:buttonid);";
        $insertResult = $DBH->prepare($insertQuery);
        $insertResult->execute(array(':buttonState' => $buttonState, ':buttonid' => $buttonid));
        if (!$insertResult) {
            echo "Error: couldn't execute query. ".$insertResult->errorCode();
            exit;
        }

        echo "Success";
        exit;
    }
?>
