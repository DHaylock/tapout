<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Tapout</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Lato:300,400,600,700,900&amp;lang=en"/>

    <link rel='stylesheet' href="./js/bootstrap/css/bootstrap.css"/>
    <script src="./js/bootstrap/js/bootstrap.js"></script>
    <script src="./js/BootstrapFormHelpers/js/bootstrap-formhelpers-phone.js"></script>
    <link rel='stylesheet' href="./js/font-awesome/css/font-awesome.css"/>
    <link rel='stylesheet' href="./js/animate.css"/>
    <style>
        .modal-body .form-horizontal .col-sm-2,
        .modal-body .form-horizontal .col-sm-10 {
            width: 100%
        }

        .modal-body .form-horizontal .control-label {
            text-align: left;
        }
        .modal-body .form-horizontal .col-sm-offset-2 {
            margin-left: 15px;
        }
    </style>
    <script src="./js/bootstrap-notify/bootstrap-notify.js"></script>
    <script src="//js.pusher.com/3.1/pusher.min.js"></script>
    <script src="https://js.pusher.com/3.1/pusher.min.js"></script>
    <script src="./js/mindmup-editable.js"></script>
    <script src="admin.js"></script>
</head>
    <body>
        <?php include("navbar.php") ?>
        <div class="sm-6">
            <table class="table">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Admin Name</th>
                        <th>Admin Mobile</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody id="tablebody">
                </tbody>
            </table>
        </div>

        <div class="sm-6">
            <table class="table">
                <thead>
                    <tr>
                        <th>User Message</th>
                        <th>Friend Message</th>
                        <th>Admin Message</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody id="messagetablebody">
                </tbody>
            </table>
        </div>
        <!-- Message Modal -->
        <div class="modal fade" id="messageModal" tabindex="-1" role="dialog" aria-labelledby="messageModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="messageModalLabel">Update Data</h4>
                    </div>
                    <div class="modal-body">
                        <form role="form" action="adminManagement.php" method="POST">
                            <div class="form-group">
                                <label for="usermessage">User Message:</label>
                                <input type="text" name="usermessage" class="form-control" id="usermessage">
                            </div>
                            <div class="form-group">
                                <label for="friendmessage">Friend Message:</label>
                                <input type="text" name="friendmessage" class="form-control" id="friendmessage">
                            </div>
                            <div class="form-group">
                                <label for="adminmessage">Admin Message:</label> <p>Hi $adminame, $username -></p>
                                <input type="text" name="adminmessage" class="form-control" id="adminmessage">
                            </div>


                            <button type="submit" name="updateMessages" value="updateMessages" class="btn btn-success">Update</button>
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            $('#messageModal').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget)
                var usermsg = document.getElementById('actualusermessage').innerHTML;
                var friendmsg = document.getElementById('actualfriendmessage').innerHTML;
                var adminmsg = document.getElementById('actualadminmessage').innerHTML;
                var modal = $(this)
                modal.find('.modal-title').text('Messages');
                modal.find('#usermessage').val(usermsg);
                modal.find('#friendmessage').val(friendmsg);
                modal.find('#adminmessage').val(adminmsg);
            })
        </script>

        <!-- Modal -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Update Data</h4>
                    </div>
                    <div class="modal-body">
                        <form role="form" action="adminManagement.php" method="POST">
                            <div class="form-group">
                                <label for="id">Id</label>
                                <input type="text" name="id" class="form-control" id="id">
                            </div>
                            <div class="form-group">
                                <label for="adminname">Admin Name:</label>
                                <input type="text" name="adminname" class="form-control" id="adminname">
                            </div>
                            <div class="form-group">
                                <label for="adminmobile">Admin Mobile:</label>
                                <input type="text" name="adminmobile" class="form-control input-medium bfh-phone" data-format="+44dddddddddd" id="adminmobile">
                            </div>
                            <button type="submit" name="update" value="update" class="btn btn-success">Update</button>
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            $('#myModal').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget)
                var id = button.data('id')
                var messageto = button.data('messageto')
                var adminname = document.getElementById('name'+id).innerHTML;
                var adminmobile = document.getElementById('mob'+id).innerHTML;
                var modal = $(this)
                modal.find('.modal-title').text('Messages');
                modal.find('#adminname').val(adminname);
                modal.find('#adminmobile').val(adminmobile);
                modal.find('.modal-title').text('Admin ' + id)
                modal.find('#id').val(id)
            });
        </script>
    </body>
</html>
