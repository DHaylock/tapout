<?php
    if (isset($_POST['update'])) {
        // Check if there is a Unique Button Id
        if (!isset($_POST['buttonId'])) {
            echo "No Button ID";
            exit;
        }
        require_once ('./tapoutcred.php');
        $buttonid = $_POST['buttonId'];
        if (!isset($_POST['ownername'])) {
            echo "No New Owner";
            exit;
        }

        if (!isset($_POST['ownermobile'])) {
            echo "No New Mobile";
            exit;
        }

        if (!isset($_POST['friendname'])) {
            echo "No New Friend Owner";
            exit;
        }

        if (!isset($_POST['friendmobile'])) {
            echo "No New Friend Mobile";
            exit;
        }

        $ownername = $_POST['ownername'];
        $ownermobile = $_POST['ownermobile'];

        $friendname = $_POST['friendname'];
        $friendmobile = $_POST['friendmobile'];

        $updateQuery = "UPDATE `buttonsowner`
                        SET `ownername`= :ownername,
                        `ownermobile`= :ownermobile,
                        `friendname`= :friendname,
                        `friendmobile`= :friendmobile
                        WHERE `buttonid`= :buttonid
                        ";

        $updateResult = $DBH->prepare($updateQuery);
        $updateResult->execute(array(':ownername' => $ownername,':ownermobile' => $ownermobile,':friendname' => $friendname,':friendmobile' => $friendmobile,':buttonid' => $buttonid));

        if (!$updateResult) {
            echo "Error: couldn't execute query. ".$updateResult->errorCode();
            exit;
        }
        header("Refresh:1; url=index.php");
        exit;
    }
    else if (isset($_POST['clear'])) {
        echo "Clear ".$_POST['buttonid'];
        exit;
    }
?>
