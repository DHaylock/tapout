// Enable pusher logging - don't include this in production
Pusher.logToConsole = false;
var pusher = new Pusher('9242beac57e0fb582791', {
    cluster: 'eu',
    encrypted: true
});

var channel = pusher.subscribe('test_channel');
channel.bind('my_event', function(data) {
    console.log(data.id);
    $('#'+data.id).text(data.state);
    $('#date'+data.id).text(data.lastused);
    var tableContent = '<tr>'+
            '<td>'+data.id+'</td>'+
            '<td>'+data.lastused+'</td>'+
            '<td>'+data.state+'</td>'+
            '</tr>';
            $("#logtablebody").prepend(tableContent);

    $.notify({
        title: "<i class='fa fa-crosshairs'></i><span class='sr-only'></span>",
        message: "Button "+data.id+" was "+data.state + "\n "+data.lastused
    },{
        placement:{
            from: "top",
            align:"center"
        }
    });
});



$.getJSON("http://tapout.davidhaylock.co.uk/getData.php?get", function(json1) {
    $.each(json1, function(key, data) {
        var tableContent =
                '<tr>'+
                '<td>'+data.buttonid+'</td>'+
                // '<td id="date'+data.buttonid+'">'+data.lastused+'</td>'+
                '<td id="name'+data.buttonid+'">'+data.ownername+'</td>'+
                '<td id="mob'+data.buttonid+'">'+data.ownermobile+'</td>'+
                '<td id="friendname'+data.buttonid+'">'+data.friendname+'</td>'+
                '<td id="friendmob'+data.buttonid+'">'+data.friendmobile+'</td>'+
                '<td>'+
                '<button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal" data-messageto="'+data.ownername+'" data-id="'+data.buttonid+'">Update</button>'+
                '</td>'+
                '</tr>';
                $("#tablebody").append(tableContent);
    });
});


function getTheData() {
    $.getJSON("http://tapout.davidhaylock.co.uk/getData.php?getLog", function(json1) {
        $.each(json1, function(key, data) {
            var tableContent = '<tr>'+
                    '<td>'+data.buttonid+'</td>'+
                    '<td>'+data.timestamp+'</td>'+
                    '<td>'+data.action+'</td>'+
                    '</tr>';
                    $("#logtablebody").append(tableContent);
        });
    });
}

getTheData();
