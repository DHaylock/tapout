<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Tapout</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Lato:300,400,600,700,900&amp;lang=en"/>

    <link rel='stylesheet' href="./js/bootstrap/css/bootstrap.css"/>
    <script src="./js/bootstrap/js/bootstrap.js"></script>
    <script src="./js/BootstrapFormHelpers/js/bootstrap-formhelpers-phone.js"></script>
    <link rel='stylesheet' href="./js/font-awesome/css/font-awesome.css"/>
    <link rel='stylesheet' href="./js/animate.css"/>
    <style>
        .modal-body .form-horizontal .col-sm-2,
        .modal-body .form-horizontal .col-sm-10 {
            width: 100%
        }

        .modal-body .form-horizontal .control-label {
            text-align: left;
        }
        .modal-body .form-horizontal .col-sm-offset-2 {
            margin-left: 15px;
        }
    </style>
    <script src="./js/bootstrap-notify/bootstrap-notify.js"></script>
    <script src="//js.pusher.com/3.1/pusher.min.js"></script>
    <script src="https://js.pusher.com/3.1/pusher.min.js"></script>
    <script src="./js/mindmup-editable.js"></script>
    <script src="tapout.js"> </script>

</head>
    <body>
        <?php include("navbar.php") ?>
        <div class="container">
            <h1>Tapout</h1>
            <p>Please do not refresh the page, it will automatically update on an event.</p>
            <h4>Overview</h4>
            <div class="sm-6">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Button Id</th>
                            <!-- <th>Last Used</th> -->
                            <th>User Name</th>
                            <th>User Mobile</th>
                            <th>Friend Name </th>
                            <th>Friend Mobile</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody id="tablebody">
                    </tbody>
                </table>
            </div>
            <h4>Log</h4>
            <div class="sm-6">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Button Id</th>
                            <th>Last Used</th>
                            <th>Button State</th>
                        </tr>
                    </thead>
                    <tbody id="logtablebody">
                    </tbody>
                </table>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Update Data</h4>
                    </div>
                    <div class="modal-body">
                        <form role="form" action="dbManagement.php" method="POST">
                            <div class="form-group">
                                <label for="ids">Id</label>
                                <input type="text" name="buttonId" class="form-control" id="ids">
                            </div>
                            <div class="form-group">
                                <label for="name">User Name:</label>
                                <input type="text" name="ownername" class="form-control" id="name">
                            </div>
                            <div class="form-group">
                                <label for="mobile">User Mobile:</label> i.e +44723456789
                                <input type="text" name="ownermobile" class="form-control input-medium bfh-phone" data-format="+44dddddddddd" id="mobile">
                            </div>
                            <div class="form-group">
                                <label for="friendname">Friend Name:</label>
                                <input type="text" name="friendname" class="form-control" id="friendname">
                            </div>
                            <div class="form-group">
                                <label for="friendmobile">Friend Mobile:</label> i.e +44723456789
                                <input type="text" name="friendmobile" class="form-control input-medium bfh-phone" data-format="+44dddddddddd" id="friendmobile">
                            </div>

                            <button type="submit" name="update" value="update" class="btn btn-success">Update</button>
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- <script type="text/javascript">
            $('#myModal').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget)
                var id = button.data('id')
                var messageto = button.data('messageto')
                var adminname = document.getElementById('name'+id).innerHTML;
                var adminmobile = document.getElementById('mob'+id).innerHTML;
                var modal = $(this)
                modal.find('.modal-title').text('Messages');
                modal.find('#adminname').val(adminname);
                modal.find('#adminmobile').val(adminmobile);
                modal.find('.modal-title').text('Admin ' + id)
                modal.find('#id').val(id)
            });
        </script> -->
        <script type="text/javascript">
            $('#myModal').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget)
                var id = button.data('id')
                var messageto = button.data('messageto')
                var username = document.getElementById('name'+id).innerHTML;
                var usermobile = document.getElementById('mob'+id).innerHTML;
                var friendname = document.getElementById('friendname'+id).innerHTML;
                var friendmobile = document.getElementById('friendmob'+id).innerHTML;

                var modal = $(this)
                // modal.find('#adminmobile').val(adminmobile);
                modal.find('.modal-title').text('Button ' + id)
                modal.find('#name').val(username);
                modal.find('#mobile').val(usermobile);
                modal.find('#friendname').val(username);
                modal.find('#friendmobile').val(usermobile);
                modal.find('#ids').val(id)
            })
        </script>
    </body>
</html>
