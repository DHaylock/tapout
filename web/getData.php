<?php
require_once ('./tapoutcred.php');
if (isset($_GET['get'])) {
    $query = "SELECT btns.buttonid,
                     btns.lastused,
                     btnown.ownername,
                     btnown.ownermobile,
                     btnown.friendname,
                     btnown.friendmobile,
                     btnown.message,
                     btns.buttonstate
                FROM `buttonsowner` AS btnown
                INNER JOIN `buttons` AS btns
                WHERE btns.buttonid = btnown.buttonid
                ";

    $result = $DBH->prepare($query);
    $result->execute();

    if (!$result) {
        echo "Error: couldn't execute query. ".$result->errorCode();
        exit;
    }

    if ($result->rowCount() == 0) {
		echo "[]";
		exit;
	}

	$rows = array();
	while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
        $w = date_create($row['lastused']);
        $we = date_format($w,"F j, Y, g:i a");
        $alteredRow = array('buttonid' => $row['buttonid'],
                            'ownername' => $row['ownername'],
                            'ownermobile' => $row['ownermobile'],
                            'friendname' => $row['friendname'],
                            'friendmobile' => $row['friendmobile'],
                            'buttonstate' => $row['buttonstate'],
                            'message' => $row['message']);
        $rows[] = $alteredRow;
	}
	echo json_encode($rows);
    exit;
}
else if (isset($_GET['getLog']))
{
    $query = "SELECT id,
                     buttonid,
                     event,
                     action,
                     timestamp,
                     UNIX_TIMESTAMP(timestamp) as timekey
                FROM `events`
                ORDER BY timekey DESC
                LIMIT 20";
    $result = $DBH->prepare($query);
    $result->execute();

    if (!$result) {
        echo "Error: couldn't execute query. ".$result->errorCode();
        exit;
    }

    if ($result->rowCount() == 0) {
		echo "[]";
		exit;
	}

	$rows = array();
	while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
        // $rows[] = $row;
        $w = date_create($row['timestamp']);
        $we = date_format($w,"F j, Y, g:i a");
        $alteredRow = array('buttonid' => $row['buttonid'],'action' => $row['action'],'timestamp' => $we);
        $rows[] = $alteredRow;
	}
	echo json_encode($rows);
    exit;
}
else if (isset($_GET['getAdmin'])) {

    $query = "SELECT * FROM `admindata`";
    $result = $DBH->prepare($query);
    $result->execute();

    if (!$result) {
        echo "Error: couldn't execute query. ".$result->errorCode();
        exit;
    }

    if ($result->rowCount() == 0) {
		echo "[]";
		exit;
	}

	$rows = array();
	while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
        $rows[] = $row;
	}
	echo json_encode($rows);
    exit;
}
else if (isset($_GET['getMessages'])) {

    $query = "SELECT * FROM `messages`";
    $result = $DBH->prepare($query);
    $result->execute();

    if (!$result) {
        echo "Error: couldn't execute query. ".$result->errorCode();
        exit;
    }

    if ($result->rowCount() == 0) {
		echo "[]";
		exit;
	}

	$rows = array();
	while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
        $rows[] = $row;
	}
	echo json_encode($rows);
    exit;
}
?>
