<?php
    $dbname = "DB_NAME";
    $dbpasskey = 'DB_PASSWORD';

    try {
        $DBH = new PDO('mysql:host=localhost;dbname='.$dbname.'',$dbname,$dbpasskey);
    } catch(PDOException $e) {
        echo $e->getMessage();
        exit;
    }
?>
