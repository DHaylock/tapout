<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Tapout Test</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Lato:300,400,600,700,900&amp;lang=en"/>
    <script src="./js/bootstrap/js/bootstrap.js"></script>
    <link rel='stylesheet' href="./js/bootstrap/css/bootstrap.css"/>
    <script src="./js/bootstrap/js/bootstrap.js"></script>
    <script type="text/javascript">
        $.getJSON("http://tapout.davidhaylock.co.uk/getData.php?get", function(json1) {
            $.each(json1, function(key, data) {
                var tableContent = '<option value="'+data.buttonid+'">Button '+data.buttonid+'</option>';
                $("#buttons").append(tableContent);
            });
        });
    </script>
</head>
    <body id="page-top" class="index">
        <?php include("navbar.php") ?>
        <div class="container">
            <div style="margin-top:10px;" class="sm-6">
                <form method="POST" action="tapped.php" class="form-signin">
                    <select id="buttons" class="form-control selectpicker" name="buttonId">
                        <!-- <option value="1">Button 1</option>
                        <option value="2">Button 2</option> -->
                    </select>
                    <div style="margin-bottom:5px;" class="sm-3">
                        <select class="form-control selectpicker" name="buttonState">
                            <option value="0">Released</option>
                            <option value="1">Pushed</option>
                        </select>
                    </div>

                    <!-- <input class="form-control" type="text" name="buttonState" placeholder="State"> -->
                    <input id="button" class="form-control btn btn-primary btn-block" type="submit" name="submit" value="submit">
                </form>
            </div>
        </div>
    </body>
</html>
