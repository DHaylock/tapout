<?php
if (isset($_POST['update'])) {
    // Check if there is a Unique Button Id
    if (!isset($_POST['id'])) {
        echo "No Button ID";
        exit;
    }
    require_once ('./tapoutcred.php');
    $adminid = $_POST['id'];
    if (!isset($_POST['adminname'])) {
        echo "No New Owner";
        exit;
    }
    $adminname = $_POST['adminname'];
    $adminmobile = $_POST['adminmobile'];

    if (!isset($_POST['adminmobile'])) {
        echo "No New Mobile";
        exit;
    }
    $updateQuery = "UPDATE `admindata`
                    SET `adminname`= :adminname,
                    `adminmobile`= :adminmobile
                    WHERE `id`= :adminid";

    $updateResult = $DBH->prepare($updateQuery);
    $updateResult->execute(array(':adminname' => $adminname, ':adminmobile' => $adminmobile, ':adminid' => $adminid));

    if (!$updateResult) {
        echo "Error: couldn't execute query. ".$updateResult->errorCode();
        exit;
    }
    // echo "Success";
    header("Refresh:1; url=admin.php");
    exit;
}
else if (isset($_POST['updateMessages'])) {
    require_once ('./tapoutcred.php');

    if (!isset($_POST['usermessage'])) {
        echo "No User Message";
        exit;
    }
    if (!isset($_POST['friendmessage'])) {
        echo "No Friend Message";
        exit;
    }
    if (!isset($_POST['adminmessage'])) {
        echo "No Admin Message";
        exit;
    }

    $usermessage = $_POST['usermessage'];
    $friendmessage = $_POST['friendmessage'];
    $adminmessage = $_POST['adminmessage'];

    $updateQuery = "UPDATE `messages`
                    SET `usermessage`= :usermessage,
                    `friendmessage`= :friendmessage,
                    `adminmessage`= :adminmessage
                    ";
    $updateResult = $DBH->prepare($updateQuery);
    $updateResult->execute(array(':usermessage' => $usermessage, ':friendmessage' => $friendmessage, ':adminmessage' => $adminmessage));
    
    if (!$updateResult) {
        echo "Error: couldn't execute query. ".$updateResult->errorCode();
        exit;
    }
    // echo "Success";
    header("Refresh:1; url=admin.php");
    exit;
}
?>
